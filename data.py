import jinja2

_icons = {
    'default':  ' ?   ',
    'inode':    '[  ]  ',
    'text':     '[=]  ',
    'audio':    '((>  ',
    'image':    '[o]  ',
    'video':    '$<  '
}
icons = {key: value.replace(' ', '&nbsp;') for key, value in _icons.items()}

_containers = {
    'audio.html': ('ogg', 'opus', 'mp3', 'flac', 'wav',),
    'video.html': ('webm', 'mp4',),
    'image.html': ('jpg', 'jpeg', 'png', 'gif',),
    'svg.html': ('svg',),
    'text.html': ('txt',),
}
containers = {value: key for key, l in _containers.items() for value in l}

templates = jinja2.Environment(
        loader=jinja2.FileSystemLoader('templates'))
