import aiohttp
from aiohttp import web
import json
import time
from uuid import uuid4
from datetime import datetime
import os

def get_extension(filename):
    s = filename.split('.')
    if len(s) > 1:
        return '.'+s[-1]
    else:
        return ''

async def handle_multipart(req,
        keep_filename=False,
        chunk_size=6*(1000**2),
        output_f=lambda fn: open(fn, 'wb')):
    reader = aiohttp.MultipartReader.from_response(req)
    output = None
    req.mp_query = {}
    size = 0
    file_size = int(req.headers.get('Content-Length', 0))
    start = time.time()
    while True:
        part = await reader.next()
        if part is None:
            break
        if part.filename is None:
            key = part.name
            value = await part.text()
            req.mp_query[key] = value
        elif len(part.filename) > 0:
            if output is None:
                if keep_filename:
                    filename = part.filename
                else:
                    ext = get_extension(part.filename)
                    now = datetime.today().strftime('%Y%m%d_%H%M%S')
                    filename = f'{now}{ext}'
                req.mp_query['filename'] = filename
                req.app.logger.debug('starting upload for {}'.format(filename))
                output = await output_f(filename)
            data = b''
            while not part.at_eof():
                chunk = await part.read_chunk(size=chunk_size)
                #print('read chunk size {}'.format(len(chunk)))
                if len(chunk) == 0:
                    break

                if len(data) == 0:
                    data = chunk
                else:
                    data += chunk

                if len(data) >= chunk_size:
                    await output.write(data)
                    size += len(data)
                    data = b''

                #print('\r{:02f}kB/{:02f}kB {:02f}kB/s'.format(size/1024, file_size/1024, size/1024/(time.time()-start)), end='')
            #print()
            if len(data) > 0:
                #print('writing chunk size {}'.format(len(data)))
                await output.write(data)
    if output:
        req.app.logger.debug('finished upload for {}'.format(filename))
        output.close()
