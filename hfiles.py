#!/usr/bin/python
import argparse
import asyncio
from aiohttp import web
import logging
import os
import uvloop
asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

from routes import setup_routes
from users import setup_users
from middlewares import setup_middlewares

def init(debug=False):

    app = web.Application()

    app.file_path = '/srv/public/'
    app.upload_path = '/srv/public/upload/'
    app.thumbnail_path = '/srv/public/.thumbnail/'
    app.meta_path = '/srv/public/.meta/'
    app.users_config = 'users.json'

    setup_routes(app)
    setup_users(app)
    setup_middlewares(app)

    if debug:
        logging.basicConfig(level=logging.INFO)
    else:
        logging.basicConfig(level=logging.INFO, filename='{}.log'.format(__name__))

    return app

def run(debug=False):
    os.setgid(1016)
    os.setuid(1000)
    if os.getuid() == 0:
        print('ROOT')
        return None

    app = init(debug)
    if debug:
        web.run_app(app, port=9092)
    else:
        web.run_app(app, port=9091)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-D', dest='debug', action='store_true', help='debug')
    args = parser.parse_args()
    run(debug=args.debug)
