import grp
import json
import os
import pwd
from uuid import uuid4

class User(dict):
    def __init__(self, fs_uid, admin=False, **kwargs):
        super(User, self).__init__()

        self.public_ids = []
        self['fs_uid'] = fs_uid
        self['admin'] = admin
        self['uid'] = kwargs.get('uid', uuid4().hex)
        self.name = pwd.getpwuid(fs_uid).pw_name
        self.fs_gids = [g.gr_gid for g in filter(lambda g: self.name in g.gr_mem, grp.getgrall())]

    def is_owner(self, filename):
        s = os.stat(filename)
        return (self['admin'] or self['fs_uid'] == s.st_uid or s.st_gid in self.fs_gids)

    def add_public_id(self):
        public_id = uuid4().hex
        self.public_ids.append(public_id)
        return public_id

class Users(dict):
    def __init__(self, filename):
        super(Users, self).__init__()
        self.filename = filename
        self['users'] = {}
        self['trusted'] = {}

        if os.path.exists(filename):
            with open(filename, 'r') as f:
                d = json.load(f)
                for k, v in d['users'].items():
                    self['users'][k] = User(**v)
                self['default'], self.default = d['default'], self['users'][d['default']]
                self['admin'], self.admin = d['admin'], self['users'][d['admin']]
                
                for remote, name in d['trusted'].items():
                    self['trusted'][remote]  = name
        else:
            self['default'] = None
            self['admin'] = None

    def get(self, key, default=None):
        return self['users'].get(key, self['default'])

    def get_uid(self, uid):
        for user in self['users'].values():
            if uid == user['uid']:
                return user
        return self.default

    def get_user(self, req):
        if req.cookies.get('login') == "0":
            return self.default
        #if req.real_ip == '127.0.0.1':
        #    return self.admin
        user = self['trusted'].get(req.real_ip)
        if user:
            return self['users'].get(user)
        return self.get_uid(req.cookies.get('uid'))

    def find_public_id(self, public_id):
        for name, user in self['users'].items():
            if public_id in user.public_ids:
                return name, user
        return None, None

    def add(self, fs_uid, admin=False):
        user = User(fs_uid, admin)
        self['users'][user.name] = user
        return user.name 

    def save(self):
        with open(self.filename, 'w') as f:
            json.dump(self, f)

def setup_users(app):
    app.users = Users(app.users_config)
