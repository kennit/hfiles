import argparse
import aiohttp
import asyncio
from bs4 import BeautifulSoup
import os

async def test_page(session, url, method='GET', status=200, **kwargs):
    if method == 'GET':
        req = session.get(url, **kwargs)
    elif method == 'POST':
        req = session.post(url, **kwargs)
    else:
        print('Unknown method', method)
        return

    async with req as res:
        if status != res.status:
            print(url, method, 'returned', res.status, 'expected', status)
        return res

def format_args(args, url):
    return [url+args[0], *args[1:]]

async def test_urls(tests, url):
    async with aiohttp.ClientSession() as session:
        await asyncio.gather(*[test_page(session, *format_args(args, url)) for args in tests])

async def recursive(session, root, host):
    url = host+root
    #print('testing', url)
    res = await test_page(session, url)
    if(url[-1] == '/'):
        text = await res.text()
        soup = BeautifulSoup(text, 'html.parser')
        d = soup.find(class_='dir')
        if d:
            await asyncio.gather(*[recursive(session, a['href'], host)\
                for a in filter(lambda a: a.string != '..', d.find_all('a'))])

async def test_recursive(root, host):
    async with aiohttp.ClientSession() as session:
        await recursive(session, root, host)

basic = [
    ('/', 'GET', 200),
    ('/view/', 'GET', 200),
    ('/view/README.txt', 'GET', 200),
    ('/files/README.txt', 'GET', 200),
    ('/view/asdf', 'GET', 404),
    ('/files/asdf', 'GET', 404),
    ('/view/message.txt', 'GET', 404),
    ('/files/message.txt', 'GET', 404),
    ('/view/test/cryptic.message', 'GET', 200),
    ('/files/test/cryptic.message', 'GET', 200),
    ('/view/test/', 'GET', 200),
    ('/view/test', 'GET', 200),
    ('/files/test/', 'GET', 200),
    ('/files/test', 'GET', 200),
    ('/view/asdf/', 'GET', 404),
    ('/files/asdf/', 'GET', 404),
    ('/view/maps', 'GET', 200),
    ('/upload', 'GET', 403),
    ('/upload', 'POST', 403),
]
host = 'http://localhost:9090'

parser = argparse.ArgumentParser()
parser.add_argument('mode', default='basic', nargs='?')
args = parser.parse_args()

if args.mode == 'basic':
    main = test_urls(basic, host)
elif args.mode == 'crawl':
    main = test_recursive('/view/', host)

asyncio.get_event_loop().run_until_complete(main)
