from aiohttp import web
from views import *

def setup_middlewares(app):
    error_middleware = error_pages({
        400: handle_400,
        403: handle_403,
        404: handle_404,
        500: handle_500})
    app.middlewares.append(real_ip())
    app.middlewares.append(error_middleware)
    app.middlewares.append(login())

def real_ip():
    @web.middleware
    async def middleware(req, handler):
        req.real_ip = req.headers.get('X-Real-IP')
        return await handler(req)
    return middleware

def error_pages(overrides):
    @web.middleware
    async def middleware(req, handler):
        try:
            res = await handler(req)
            override = overrides.get(res.status)
            if override is None:
                return res
            else:
                return await override(req, res)
        except web.HTTPException as ex:
            override = overrides.get(ex.status)
            if override is None:
                raise
            else:
                return await override(req, ex)
    return middleware

def login():
    @web.middleware
    async def middleware(req, handler):
        req.user = req.app.users.get_user(req)
        return await handler(req)
    return middleware
        
