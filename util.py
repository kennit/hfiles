import json
import math
import mimetypes
import os
import urllib.parse

from data import icons, containers, templates
from thumbnail import thumbnail

def get_login(req):
    if req.cookies.get('uid') is not None or req.remote in req.app.users['trusted']:
        return req.cookies.get('login', '0')
    return None

def safe_filename(filename):
    if not filename:
        return ''
    if filename[0] == '/':
        filename = filename[1:]
    if len(filename) > 2 and filename[:2] == '..':
        filename = filename[2:]
    if filename[0] == '/':
        filename = filename[1:]
    return filename.rstrip('/')

def file_object_from_req(req, **kwargs):
    rel_filename = '/'+safe_filename(req.match_info.get('filename', ''))

    return file_object(rel_filename, req, **kwargs)

def file_object(rel_filename, req, path=None, **kwargs):
    if path is None:
        path = req.app.file_path
    filename = os.path.join(path, rel_filename[1:])
    #print(rel_filename, filename)

    if os.path.isdir(filename):
        if len(rel_filename) > 1:
            rel_filename = rel_filename + '/'
            filename = filename + '/'

        return Directory(rel_filename, filename, req)
    else:
        return File(rel_filename, filename, req)

class FileObject:
    def __init__(self, rel_filename, filename, req, mode='view', **kwargs):
        self.rel_filename = rel_filename
        self.filename = filename
        self.req = req
        self.mode = mode

        self.name = rel_filename.rstrip('/').split('/')[-1]

    @property
    def url(self):
        return urllib.parse.quote('/files'+self.rel_filename)

    @property
    def view(self):
        return urllib.parse.quote('/view'+self.rel_filename)

    @property
    def href(self):
        if self.mode == 'raw':
            return self.url
        elif self.mode == 'view':
            return self.view

    @property
    def parent(self, **kwargs):
        if self.filename == self.req.app.file_path:
            rel_filename = '/'
        else:
            rel_filename = '/'.join(self.rel_filename.rstrip('/').split('/')[:-1])
            if rel_filename == '':
                rel_filename = '/'
        #print('parent of', self.rel_filename, 'is', rel_filename)
        return file_object(rel_filename, self.req, **kwargs)

    @property
    def neighbors(self):
        i = 0
        siblings = [*filter(lambda f: isinstance(f, File), self.parent.children())]
        for i, f in enumerate(siblings):
            if self.name == f.name:
                break 
        if i == len(siblings)-1:
            return siblings[i-1], siblings[0]
        else:
            return siblings[i-1], siblings[i+1]

class File(FileObject):
    def __init__(self, *args, **kwargs):
        super(File, self).__init__(*args, **kwargs)

        t = mimetypes.guess_type(self.name)
        if t is not None and t[0] is not None:
            t = t[0]
            if 'text' in t:
                t += '; charset=utf-8'
            self.content_type = t
        else:
            self.content_type = 'application/octet-stream'

        self.icon = icons.get(self.content_type.split('/')[0], icons['default'])
        self.file_type = containers.get(self.rel_filename.split('.')[-1].lower(), '')
        if 'image' in self.file_type and not self.filename.startswith(self.req.app.thumbnail_path):
            self._thumbnail = None

    @property
    def size(self):
        s = os.path.getsize(self.filename)
        suffix = ('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB')
        i = 0
        if s >= 1024:
            i = int(math.floor(math.log(s, 1024)))
            p = math.pow(1024, i)
            s /= p
        return '{:.2f}{}'.format(s, suffix[i])

    @property
    def container(self):
        if self.file_type is '':
            return None
        return templates.get_template(self.file_type).render(f=self)

    @property
    def preview(self):
        if self.file_type is '':
            return None
        try:
            t = templates.get_template(self.file_type.split('.')[0]+'_preview.html')
        except Exception:
            t = templates.get_template(self.file_type)
        return t.render(f=self)

    @property
    def thumbnail(self):
        if self._thumbnail:
            return self._thumbnail

        filename = os.path.join(self.req.app.thumbnail_path, self.rel_filename.lstrip('/'))
        if not os.path.exists(filename):
            parent = os.path.join(self.req.app.thumbnail_path, self.parent.rel_filename.lstrip('/'))
            #print(filename, parent)

            if not os.path.exists(parent):
                os.makedirs(parent)

            print('making thumbnail', filename)
            try:
                thumbnail(self.filename, filename)
                print('made thumbnail', filename)
            except Exception as e:
                print(e)

        thumbnail_url = filename.replace(self.req.app.file_path, '/')
        self._thumbnail = file_object(thumbnail_url, self.req, thumbnail=True)

        return self._thumbnail

    @property
    def text(self):
        with open(self.filename, 'r') as f:
            try:
                _text = f.read()
            except Exception as e:
                print(e)
                return ''
            return _text
    @property
    def text_preview(self):
        _text = self.text
        split = _text.split('\n')
        max_len = 28
        if len(split) > max_len:
            _text = '\n'.join((*split[:max_len], '...'))
        return _text

class Directory(FileObject):
    def __init__(self, *args, sort='name', reverse=False, **kwargs): 
        super(Directory, self).__init__(*args, **kwargs)
        self.sort = sort
        self.reverse = reverse
        self.icon = icons['inode']
        self.container = None

    def list(self):
        meta_filename = os.path.join(self.req.app.meta_path, self.rel_filename[1:], 'meta.json')
        if os.path.exists(meta_filename):
            with open(meta_filename, 'r') as f:
                options = json.load(f)
        else:
            options = {
                'reverse': False,
                'priority': 'directory' 
            }

        files = filter(lambda f: f[0] != '.' and self.req.user.is_owner(os.path.join(self.filename, f)), os.listdir(self.filename))

        sorter = lambda l: sorted(l, reverse=options['reverse'])

        if options['priority'] == 'directory' or options['priority'] =='files':
            dirs, fs = [], []
            for f in files:
                if os.path.isdir(os.path.join(self.filename, f)):
                    dirs.append(f)
                else:
                    fs.append(f)

            if options['priority'] == 'directory':
                files = [*sorter(dirs), *sorter(fs)]
            elif options['priority'] == 'files':
                files = [*sorter(fs), *sorter(dirs)]
        else:
            files = sorter(files)
        return files

    def children(self, **kwargs):
        for name in self.list():
            yield file_object(os.path.join(self.rel_filename, name), req=self.req, **kwargs)
