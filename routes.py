from views import *
def setup_routes(app):
    app.router.add_get('/', index)
    #app.router.add_get(r'/files/{filename:.+}', get_file)
    app.router.add_get('/view/', view_dir) 
    app.router.add_get(r'/view/{filename:.+}/', view_dir)
    app.router.add_get(r'/view/{filename:.+}', view_file)
    app.router.add_get('/upload', get_upload)
    app.router.add_post('/upload', upload)
    app.router.add_get('/add_public_id', add_public_id)
    app.router.add_get('/register', register)
    app.router.add_get('/login', login)
    app.router.add_get('/logout', logout)
