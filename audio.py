import asyncio
import os
import subprocess

async def run(*args):
    process = await asyncio.create_subprocess_exec(
            *args,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE)

    print('[{}] {}'.format(process.pid, args))

    stdout, stderr = await process.communicate()

    print('[{}] returned {}'.format(process.pid, process.returncode))
    print('[{}] \"{}\"'.format(process.pid, stdout.decode().strip()))
    print('[{}] \"{}\"'.format(process.pid, stderr.decode().strip()))

async def play_all(files, outfile):
    args = ['ffmpeg', 
            '-i', 'concat:{}'.format('|'.join(filename for filename in files)),
            '-acodec', 'copy',
            outfile]

    await run(*args)

if __name__ == '__main__':
    asyncio.get_event_loop().run_until_complete(play_all(sorted(os.listdir('.')), 'out.mp3'))
