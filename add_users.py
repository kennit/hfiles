import httpf

app = httpf.init()

app.users['admin'] = app.users.add(1000, admin=True)
app.users['default'] = app.users.add(1012)
app.users.add(1011)

app.users.save()
