import aiofiles
from aiohttp import web
from util import *
from mp import handle_multipart
from datetime import datetime

async def handle_400(req, res):
    return web.HTTPNotFound(
            text=templates.get_template('error.html').render(
                code=400,
                text='Bad request: {}'.format(res.text)),
            content_type='text/html')

async def handle_403(req, res):
    return web.HTTPForbidden(
            text=templates.get_template('error.html').render(
                code=403,
                text='Forbidden'),
            content_type='text/html')

async def handle_404(req, res):
    if not hasattr(res, 'text') or res.text == '404: Not Found':
        res.text = str(req.rel_url)
    return web.HTTPNotFound(
            text=templates.get_template('error.html').render(
                code=404,
                text='{} not found'.format(res.text)),
            content_type='text/html')

async def handle_500(req, res):
    return web.HTTPForbidden(
            text=templates.get_template('error.html').render(
                code=500,
                text='Internal error'),
            content_type='text/html')

async def index(req):
    return web.Response(
            text=templates.get_template('index.html').render(
                username=req.user.name,
                login=get_login(req)),
            content_type='text/html')

async def get_upload(req):
    #if req.user == req.app.users.default:
    #    raise web.HTTPForbidden()

    return web.Response(
            text=templates.get_template('upload.html').render(
                username=req.user.name,
                login=get_login(req)),
            content_type='text/html')

async def get_file(req):
    f = file_object_from_req(req)

    if not os.path.exists(f.filename):
        raise web.HTTPNotFound()
    if not isinstance(f, File): 
        return web.HTTPFound(f.view)

    if not req.user.is_owner(f.filename):
        print(req.rel_url, 'is not owned by', req.user.name)
        raise web.HTTPNotFound()

    stream = web.StreamResponse()
    stream.content_type = f.content_type
    stream.content_length = os.path.getsize(f.filename)
    await stream.prepare(req)

    async with aiofiles.open(f.filename, 'rb') as f:
        async for chunk in f:
            await stream.write(chunk)
    await stream.write_eof()

    return stream

async def view_file(req):
    f = file_object_from_req(req)
    f.mode = 'raw'

    if not os.path.exists(f.filename):
        raise web.HTTPNotFound()
    if not isinstance(f, File): 
        return web.HTTPFound(f.view)
    if not req.user.is_owner(f.filename):
        print(req.rel_url, 'is not owned by', req.user.name)
        raise web.HTTPNotFound()

    prev, nxt = f.neighbors

    return web.Response(
            text=templates.get_template('view.html').render(
                icons=icons,
                username=req.user.name,
                login=get_login(req),
                back=f.parent.view,
                f=f,
                prev=prev,
                nxt=nxt,),
            content_type='text/html')

async def view_dir(req):
    d = file_object_from_req(req)

    if not os.path.exists(d.filename):
        print(d.filename, 'does not exist')
        raise web.HTTPNotFound()
    if not isinstance(d, Directory): 
        print(req.rel_url, 'is file')
        raise web.HTTPNotFound()
    if not req.user.is_owner(d.filename):
        print(req.rel_url, 'is not owned by', req.user.name)
        raise web.HTTPNotFound()
    
    return web.Response(
            text=templates.get_template('dir.html').render(
                icons=icons,
                files=d.children(),
                back=d.parent.view,
                username=req.user.name,
                login=get_login(req)),
            content_type='text/html')

async def add_file(req, filename):
    filename = os.path.join(req.app.upload_path, safe_filename(filename))

    if os.path.exists(filename):
        i = 0
        while True:
            fn = '{}-{}'.format(filename, i)
            if not os.path.exists(fn):
                fo.filename = fn
                break

    return await aiofiles.open(filename, 'wb')

async def upload(req):
    #if req.user == req.app.users.default:
    #    raise web.HTTPForbidden()

    await handle_multipart(req, output_f=lambda fn: add_file(req, fn))
    text = req.mp_query.get('text')
    if text:
        filename = '{}.txt'.format(datetime.today().strftime('%Y%m%d_%H%M%S'))
        f = await add_file(req, filename)
        await f.write(bytes(text, 'utf-8'))

    return web.HTTPFound('/view/upload/')

async def add_public_id(req):
    if req.real_ip != '127.0.0.1':
        raise web.HTTPForbidden()

    print(req.query.get('user'))
    user = req.app.users.get(req.query.get('user'))
    print(user)

    if user is None:
        raise web.HTTPBadRequest(text='User {} not found'.format(uid))

    public_id = user.add_public_id()
    url = '/register?id={}'.format(public_id)
    return web.Response(text=url)

async def register(req):
    public_id = req.query.get('id')
    if not public_id:
        raise web.HTTPBadRequest(text='No ID specified')
    name, user = req.app.users.find_public_id(public_id)
    if name is None:
        raise web.HTTPBadRequest(text='Invalid ID')

    user.public_ids.remove(public_id)

    print('registered as', name, user['uid'])

    res = web.HTTPFound('/login',
            headers={'Referer': req.headers.get('Referer', '/view/')})
    res.set_cookie('uid', user['uid'])

    return res

async def login(req):
    if req.cookies.get('uid'):
        res = web.HTTPFound(req.headers.get('Referer', '/view/'))
        res.set_cookie('login', 1)
        print('login as', req.app.users.get_user(req).name, req.cookies.get('uid'))
        return res

    name = req.app.users['trusted'].get(req.real_ip)
    if name is None:
        raise web.HTTPForbidden()

    user = req.app.users['users'][name]
    
    return web.HTTPFound('/register?id={}'.format(user.add_public_id()),
            headers={'Referer': req.headers.get('Referer', '/view/')})

async def logout(req):
    res = web.HTTPFound(req.headers.get('Referer', '/view/'))
    res.set_cookie('login', 0)
    return res
