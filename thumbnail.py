from PIL import Image, ImageFile
Image.MAX_IMAGE_PIXELS = None
ImageFile.LOAD_TRUNCATED_IMAGES = True

def thumbnail(infile, outfile):
    im = Image.open(infile)
    im.thumbnail((512, 1024))
    im.save(outfile)
